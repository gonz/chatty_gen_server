# ChattyGenServer

## Installation

  1. Add `chatty_gen_server` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:chatty_gen_server, git: "https://gitlab.com/gonz/chatty_gen_server.git"}]
    end
    ```

## Usage
Run `mix chatty_gen_server AppName.SubModule.ExampleServer` or the like
to generate a chatty gen server from the template. Be sure to do this in your
app root folder.
