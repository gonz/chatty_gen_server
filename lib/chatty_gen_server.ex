defmodule Mix.Tasks.ChattyGenServer do
  use Mix.Task

  @shortdoc"Creates a gen server that logs on unrecognized messages"

  def run([module_name]) do
    %{module_path: path, module_content: content} = module_data(module_name)

    File.mkdir_p! Path.dirname path
    case File.exists? path do
      true ->
        IO.puts "No file generated. '#{path}' exists."

      false ->
        File.write! path, content
        IO.puts "Generated '#{path}'"
    end
  end

  defp module_data(module_name) do
    priv_dir = Application.app_dir :chatty_gen_server, "priv"
    template_file = Path.join [priv_dir, "chatty_gen_server.ex.eex"]
    module_path =
      case Mix.Project.umbrella? do
        true ->
          [app_name | submodules] =
            module_name
            |> String.split(".")
          app_name = Macro.underscore(app_name)
          rest_path =
            submodules
            |> Enum.join(".")
            |> Macro.underscore()
          Path.join ["apps", app_name, "lib", app_name, rest_path <> ".ex"]

        false ->
          Path.join ["lib", Macro.underscore(module_name) <> ".ex"]
      end
    module_content = EEx.eval_file template_file, module_name: module_name

    %{module_path: module_path, module_content: module_content}
  end
end
